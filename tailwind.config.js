module.exports = {
    mode: 'jit',
    content: [
        './node_modules/tw-elements/js/**/*.js',
        './flask_wineshop/templates/*.jinja2',
        './flask_wineshop/static/src/js/*.js',
        './flask_wineshop/home/templates/*.jinja2',
        './flask_wineshop/account/templates/*.jinja2',
        './flask_wineshop/auth/templates/*.jinja2',
        './flask_wineshop/cart/templates/*.jinja2',
        './flask_wineshop/products/templates/*.jinja2'
    ],
    plugins: [require("tw-elements/plugin.cjs")],
    darkMode: false,
    theme: {
        colors: {
            'wine-red': '#972f1b',
        },
        fontFamily: {},   // TODO: Possibly, to load google fonts here instead of by template partials //
    },
}